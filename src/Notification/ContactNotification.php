<?php
	
	namespace App\Notification;
	
	use App\Entity\Contact;
	use Symfony\Component\Mailer\MailerInterface;
	use Symfony\Component\Mime\Email;
	use Twig\Environment;
	
	class ContactNotification
	{
		/**
		 * @var Environment
		 */
		private $renderer;
		
		/**
		 * @var Environment
		 */
		private $mailer;
		
		/**
		 * ContactNotification constructor.
		 *
		 * @param MailerInterface $mailer
		 * @param Environment     $renderer
		 */
		public function __construct(MailerInterface $mailer, Environment $renderer) {
			$this->mailer = $mailer;
			$this->renderer = $renderer;
		}
		
		
		/**
		 * @param Contact $contact
		 *
		 * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
		 * @throws \Twig\Error\LoaderError
		 * @throws \Twig\Error\RuntimeError
		 * @throws \Twig\Error\SyntaxError
		 */
		public function notify(Contact $contact){
			$message = (new Email())
				->from('b9a284a157-4ea95a@inbox.mailtrap.io')
				->to('contact@agence.fr')
				->replyTo($contact->getEmail())
				
				->subject('Agence : ' . $contact->getProperty()->getTitle())
				->text($this->renderer->render('emails/contact.html.twig',[
					'contact' => $contact]), 'text/html')
				;

			$this->mailer->send($message);
		}
		
	}